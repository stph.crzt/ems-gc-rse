# stph

## coût/qualité/délais

## plan
- une partie pour chaque valeur
- eudémonisme : prendre le temps sur l'utilitarisme de Mill

## archipel

## posture épistémo


"bricolé à partir de" SHS / technique / alter + politique / contestation
design / ou bricolé à partir du design
ingénierie
bricole l'hétérogène contre

## radicalité

exemplarité :
préfigurativité
prospectivité
utopie de More
radicalité



→ **communauté** ?

utopie : eutopie

commun /  / communalité / collectivité / collectivisation / collectif

utopie social coopération  construction

impact

équilibre ?
explicités, discutés et critiqués
sobriété médiété


https://archipel.inria.fr/charte/

Article 6 : Archipel reconnaît la non neutralité de la science et la dimension intrinsèquement politique de ces enjeux qui nécessitent donc d’être explicités, discutés et critiqués.

Article 7 : La communauté Archipel souhaite faire face d’une manière réellement systémique aux risques de déclin ou d’effondrement des sociétés modernes.  A ce titre les concepts de croissance verte et de développement durable font écran à la gravité de la situation et aux facteurs qui en sont à l’origine.


## Innovation et invention enfermées entre croissance économique et progrès technique

Le solutionnisme technologique est fondamentalement couplé avec les valeurs dominantes de nos sociétés contemporaines, que l'on peut résumer par le couplage de la croissance économique et du progrès technique.

[schéma]

### invention

L'invention a pour objet la production de nouveaux possibles techniques par la maîtrise des phénomènes naturels (sciences physiques, chimiques, biologiques...). Elle tend donc à de nouvelles prises des humains sur la nature via la technique
On peut définir formellement l'invention via le droit comme quelque chose qui peut-être protégé par un brevet. Cela revient à :
- être une solution technique à un problème technique
- comporter un caractère novateur
- impliquer une activité inventive
- être susceptible d’application industrielle.
https://www.economie.gouv.fr/entreprises/depot-brevet-inpi

### Innovation
Définition
L'innovation
a pour objet la production de nouveaux modes de vie par le développement de l'usage d'objets
techniques,

elle est associée à l'idée de croissance,
elle tend à une aliénation de l'humain par la technique : nouvelles prises des humains sur les humains via la technique
Ingénierie : exploitation de nouvelles idées (donc des inventions) pour en faire des biens et des
services commercialisables (lien à la croissance)

Philosophie : élan vital (Bergson) ; volonté de puissance (Nietzsche)
Économie : figure de l'entrepreneur (Schumpeter)

## rapport à la nature
elle tend à une domination de la nature par l'humain via la technique.

## lt et industrie

# besoins

## Paradigmes de Khun

#### La technique n'est pas neutre (choisir de donner un sens à la technique)

Ellul
Steiner

La technique n'est ni bonne, ni mauvaise, ni neutre (Thèse TAC)
Donc on peut en débattre (ni bonne, ni mauvaise)
Donc on doit en débattre (non neutre)
Il n'y a pas de solutions, il n'y a que des directions


#### #### Zéro carbone

La simplification des enjeux écologiques à la prise en compte des "impacts carbone" est une illustration d'une rémanence solutionnniste. L'impact carbone est aujourd'hui le plus facile à mesurer, il existe des outils d'ACV (Analyse du Cycle de Vie) qui permettent aux concepteurs d'évaluer l'impact d'un objet, notamment en terme d'impact carbone. Or ce geste procède en général de plusieurs simplification :

Cela permet de conclure à l'affichage d'un impact zéro carbone.

Compensation

Pensée binaire "Vert"

ACV et objectivation

[schéma stph à améliorer]


Non prise en compt^ndirects (ex : effet rebond)
Mauvaise échelle de temps, si on reconnaît par ailleurs l'urgence d'agir pour inverser les courbes
(ex : captation carbone)
Non prise en compte des effets de généralisation, à l'échelle mondiale (ex : nucléaire)

La compensation carbone
Exemple
Les principes de neutralité et de compensation carbone sont dangereux car :
ils entretiennent la pensée que des solutions de captation permettent de maintenir les activités
émettrices telles qu'elles existent,
or les surfaces de replantation d'arbres ou les coûts en énergie d'autres solutions rendent cette
perspective peu crédible,
et ils empêchent par ailleurs de penser la sobriété.


## prolétarisation

## pharmakon

La « puissance curative autant que destructrice de la technique, permet de prendre soin et (…) dont il faut prendre soin, au sens où il faut y faire attention » (L’emploi est mort, vive le travail ! Mille et une nuits, 2015).

(#### Faire des choix technologiques : il n'y a pas d'humain no-tech : découper en 3, pharmakon, TAC, médiété et ataraxie)

# évaluation


# positionnement

Ingénierie informatique (méthodologies de conception, design)

La construction méthodologique que nous proposons emprunte aux sciences humaines et notamment à :
- la philosophie de la technique : non-neutralité de la technique (Steiner), concept de soin (Guchet), convivialité (Illich), redirection (Monnin)...
- à l'histoire de la technique (Carnino, Diamond, Ellul, Masutti, Jarrige)...
- à l'anthropologie (Leroi-Gourhan, Goody, Graeber)
- à l'économie (Jevons, Raworth, Ostrom, Parrique)
- au design (Roussilhe)

# misc

Efficience versus sobriété
Exemple
Efficience : diminuer la consommation de ressources nécessaire pour atteindre un objectif (on
modifie les moyens, on ne remet pas en cause les fins)
Sobriété : modération des objectifs visés en afin de réduire la consommation de ressources (on
remet en cause les fins et les moyens)
Exemple
Produire des objets durables (un ordinateur ou un smartphone qui dure 50 ans).
Remettre en cause les modes de vie consommateurs en énergie (vidéos basse résolution, écrans
modestes, offre relocalisée).
Limiter les technologies asservissantes (disposer de services numériques locaux soumis au
contrôle démocratique plutôt que de services globaux maîtrisés par des géants internationaux).
Redonner de la place aux relations humaines (indépendance des services publics au
numérique).
L'innovation n'est pas uniquement un processus économique, mais est soumise
à des orientations politiques
Remarque
« Ici, l’argument qui nous a été opposé est donc le suivant : en tant qu’entrepreneur, je réponds à un
besoin. La preuve, mon produit est acheté par des clients donc cela suffit à valider son existence. »
Deutsch, 2021 Deutsch, 2021 p.25


# bricolage

## radicalité
remettre en cause
les modes de vie

# outils d'éval réflexif a priori




[TODO Liste plus large]

# complexité (irréductibilité)

développer & ref

# critique lt
- prospective
- subjective
- rationnalisante (utilitarisme)
- domaines limites, santé (cf illich)) militaire