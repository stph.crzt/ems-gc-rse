# Numérique responsable et lowtechisation (titre du chapitre)

## Lowtechisation

### Préambule : les 4 scénarios de l'ADEME

L'ADEME (2022) propose quatre scénarios qui permettent tous les quatre d'arriver à la neutralité carbone en 2050, c'est à dire à un « équilibre entre des flux annuels d’émissions et des flux d’absorption ». Le travail de l'ADEME se limite à la France métropolitaine. Il se limite aussi à la question de l'empreinte carbone, qui n'est qu'une des limites planétaires identifiées et qui n'est pas la seule importante, a fortiori dans le domaine du numérique où la question de l'exploitation des minerais est fondamentale (Allard, 2023 ; Pitron, 2021). Mais il s'agit d'un travail conséquent (688 pages, une centaine de contributeurs, 2 ans d'étude), complet (l'étude adresse le bâtiment, le transport, l'alimentation, l'industrie, l'énergie, les déchets), réfutable (les modes de calcul sont expliqués et les données mobilisées sont disponibles) et dynamique (le travail se complète, notamment grâce aux "feuilletons", par exemple celui sur les "matériaux de la transition" énergétique").

Ce travail s'inscrit dans une démarche de prospective, il ne prétend pas prédire l'avenir, mais c'est un bon outil pour penser la direction que l'on souhaite donner à nos développements technologiques. En particulier la formulation des 4 scénarios permet de bien mettre en exergue la possibilité d'un choix et la non neutralité de ce choix.

Schéma : https://framagit.org/stph.crzt/lownum/-/blob/main/schemas/lowtechnicisation-ademe.svg

Légende : Interprétation des scénarios de l'ADEME sur un axe lowtechisation / technosolutionnisme

Nous n'entrerons pas dans les détails de ces propositions, et ne retiendrons que les 2 scénarios les plus éloignés : le scénario S4, dit "pari réparateur" est un scénario de solutionnisme technologique, il mise sur des découvertes à venir qui permettront de résoudre les problèmes identifiés aujourd'hui (sans remettre en cause le diagnostic sur ces problèmes). Le scénario S1 de l'autre côté, "sobriété contrainte", s'articule autour de l'idée de restreindre nos développements technologiques, articulé à un scénario de décroissance d'un point de vue économique. Nous proposons de nous inscrire également dans ce cadre S1 du point de vue de l'ingénieur et de parler de lowtechisation.

### Critique du solutionnisme technologique

« A technological fix, technical fix, technological shortcut or (techno-)solutionism refers to attempts to use engineering or technology to solve a problem (often created by earlier technological interventions). » (wikipedia.org)

Avant d'entrer dans les détails de la contribution que nous proposons sous le terme de lowtechisation, revenons sur le scénario S4. En effet, celui-ci présente un avantage indéniable sur tous les autres : il promet de résoudre nos problèmes environnementaux, sans toucher (ou presque) à nos modes de vie. Nous continuerons à manger presque autant de viande, nous nous déplacerons encore plus dans un monde encore plus mondialisé, et le numérique connaîtra une croissance encore accélérée. Ce scénario ne nie aucunement la gravité des problèmes environnementaux que les scientifiques relèvent, mais il mise, littéralement, sur l'émergence de progrès techniques qui résoudront ces problèmes (géoingénierie, énergies alternatives, substitution numérique...).

#### Progrès : la technique est bonne

« Ce mot, qui signifie "marche en avant", désigne de façon toute spéciale, dans le langage philosophique, la marche du genre humain vers la perfection, vers son bonheur. (...) L'humanité est perfectible et elle va incessamment du moins bien au mieux, de l'ignorance à la science, de la barbarie à la civilisation. » (Larousse, 1890)

Cette définition du progrès rapportée dans le Grand Dictionnaire universel du XIXe siècle (1866-1876) s'inscrit dans la philosophie du positivisme logique d'Auguste Comte. Elle marque l'association, voire l'assimilation du bonheur au progrès d'une part, et du progrès à la science d'autre part. Aucune rigueur scientifique, pourtant, ne porte cette assertion. Le positivisme ne définit pas le bonheur et on ne dit pas comment se mesure la distance à la perfection. On ignore les effets délétères de la technique, déjà observables au XIXe siècle (la pollution des villes, les conditions de vie ouvrières, etc.). Guillaume Carnino (2015) défend ainsi, avec une perspective historique, que science et progrès relèvent de mécanismes dogmatiques et de sacralisation analogues à ceux de la religion qu'ils sont sensés destituer. Finalement, Le XXe siècle mettra en évidence que le progrès technique (gaz, motorisation, nucléaire...) n'est pas si certainement source de bonheur et de perfection, donnant naissance au courant technocritique, que l'on peut illustrer par le propos de Jacques Ellul (1965) : la technique n'est ni bonne, ni mauvaise, ni neutre. 

#### Croissance économique, plus c'est mieux

La croissance économique désigne la variation de la production de biens et de services. Elle est mesurée en pratique par la variation du PIB. Cette variation est généralement considérée comme bonne si elle est positive et mauvaise sinon (on parle alors de décroissance), la majorité des états cherchent à obtenir la croissance la plus importante possible. La recherche de croissance est donc associée à la croyance que l'augmentation de la production et de la consommation d'objets est un indicateur de bien-être. Pourtant, si certains objets améliore les conditions de vie de certains humains, d'autres les détériorent. Or la croissance est alimentée par tout type de production, sans aucune discrimination, aliments ou poisons, marteaux ou canons. La croissance de n'importe quoi est de la croissance.

Par ailleurs la mesure du PIB se limite aux biens et services qui reposent sur un échange économique, il ne compte ni le troc (relativement marginal dans nos sociétés), ni surtout les échanges de services gratuits (services rendus au seins de la famille, entre amis, voisins...). Donc la croissance profite d'un déplacement du temps disponible pour rendre des services non marchands vers des services rémunérés, quelque soit la nature de ces services. Passer moins de temps à l'éducation de ses enfants ou à la gestion d'un potager, pour passer plus de temps à fabriquer de la publicité pour des cigarettes ou concevoir des produits financiers spéculatifs est bénéfique à la croissance. 

À sa décharge, Timothée Parrique (2022) nous rappelle que le calcul du PIB n'a pas été conçu pour ce pour quoi il est utilisé aujourd'hui. Simon Kuznet crée cet indicateur en 1932 aux États-Unis, dans le contexte post-crise de 1929, avec l'objectif d'évaluer l'efficacité des interventions publiques en terme de relance économique. Presqu'un siècle plus tard le monde que nous connaissons est bien différent et il est probablement plus que temps de reléguer cet outil aux manuels d'histoire.

#### Un pari incertain

La première critique que l'on peut adresser au solutionnisme, est qu'il est avant tout un pari — l'ADEME parle, rappelons-le de "pari réparateur" — et ce pari est risqué. L'histoire récente montre que le solutionnisme dysfonctionne régulièrement, en particulier lorsqu'il s'agit de problèmes environnementaux.

Les Meadows formulent en 1972 les limites d'une croissance exponentielle que nous continuons pourtant de rechercher intentionnellement. Jevons formule le paradoxe de l'effet rebond dès 1865 et pour autant nous en sommes toujours presque systématiquement victime (retenons par exemple que les sources d'énergie, charbon, gaz, pétrole, nucléaire ou renouvelables, tendent à s'additionner plutôt qu'à se substituer les unes aux autres). Ainsi les solutions proposées pour le moment tendent à aggraver le problème plus qu'à le résoudre par effets indirects. Philippe Bihouix (2014) souligne le paradoxe de la situation actuelle, le monde n'a jamais été globalement aussi "saccagé" et pourtant nous n'avons jamais eu autant de "moyens techniques de régler tout ça".

#### Une pensée hégémonique

On peut critiquer l'hégémonie du solutionnisme au sein de nos sociétés comme unique scénario exploré (sans même le critiquer en tant que tel). Le solutionnisme correspond en effet à la façon de penser historiquement, techniquement, et géopolitiquement constituée de nos sociétés industrielles, renforcée notamment par les institutions publiques et privées de la recherche scientifique (Scott, 2011). Jean Jouzel (2023) critique le "rassurisme" qu'il véhicule, « sur le fait qu’on va pouvoir s’adapter, même en conservant nos modes de vie actuels, grâce à des solutions technologiques ». Le solutionnisme produit donc une forme de conservatisme qui perpétue l'espoir d'une solution miraculeuse et diminue le sentiment d'urgence, conduisant à une attitude de type "burn now, pay later" (Dyke, Watson, Knorr, 2021), puisqu'il reste toujours une possibilité que le problème se règle, en quelque sorte de lui-même. Le solutionnisme est donc dominant, il faut se "forcer" pour penser autrement, et hégémonique, il tend à affaiblir les autres façons de penser.

L'ADEME en formulant ses 4 scénarios propose une logique de délibération plutôt que de normativité, en cela elle rompt avec le solutionnisme comme pensée hégémonique.

#### Une pensée simplificatrice

La pensée cartésienne fait la part belle à l'analyse c'est à dire à l'idée qu'on peut résoudre un problème d'envergure en le découpant en sous-problèmes plus simples. Or d'une part la complexité ne se laisse pas réduire, c'est à dire que même si les sous-problèmes sont résolus, cela n'épuise pas le problème général dont une partie repose sur l'articulation entre les sous-problèmes ; la complexité se cache dans les interstices en quelque sorte.

Une des hypothèses que nous formulons est que le solutionnisme peine à intégrer les phénomènes complexes comme les effets indirects (Roussilhe, 2022) ou les approches systémiques (Hersh, 2015), pourtant indispensables dans le contexte d'une crise socio-écologique multifactorielle. 

#### Une pensée conservatrice

L'approche solutionnisme porte en elle une dimension tautologique : en postulant la solution, elle la fait pré-exister. Par ce geste elle rend difficile la remise en cause de la question initiale, elle empêche de penser le problème différemment. En discutant d'emblée de la solution que l'on cherche, on ne discute pas de ce qu'on fait et pourquoi.

Les démarches d'ingénierie comportent, en théorie des phases de questionnement du "besoin" ou des fonctions rendues par l'objet ou le service, mais en pratique le tropisme technique des ingénieurs couplé à la logique de croissance conduit à éviter de "ne pas faire" ou "à faire moins". En se confinant à la sphère technique, l'ingénieur se met et/ou est mis en posture de ne pas adresser les problèmes sociaux ou politique.

#### Une pensée mystique (de l'hubris)

« Nous ne voulons pas que le nombre d'habitants cesse de croître. Nous voulons que chacun puisse consommer toujours plus d'énergie. » et « Si nous étions un trillion d'êtres humains dans le système solaire, nous aurions un millier d'Enstein et un millier de Mozart. » sont deux citations de Jeff Bezos, le patron d'Amazon rapporté dans Socialter (Gautier, 2020). La traduction est certainement fautive et Jeff Bezos parle probablement de 1000 milliards plutôt qu'un milliard de milliards, mais, dans les deux cas, admettons que c'est beaucoup. Cette manifestation est caractéristique de l'hubris, une démesure, un désir de toujours plus, que le Second manifeste convivialiste (2020) invite à maîtriser. 

Quand le président de la France affirme affirme en 2023 qu'il « adore la bagnole », en empruntant au vocabulaire religieux il fait sortir de fait la question du débat rationnel et républicain, il ne s'agit déjà plus de discuter de la pertinence de telle croissance ou de telle progrès technique, il s'agit uniquement que des hommes puissants les désirent et y croient.

#### L'exemple de la voiture électrique

L'émergence de la voiture électrique pour résoudre les problèmes posés par la propulsion thermique est un bon exemple d'approche solutionniste.

En changeant la motorisation, la voiture électrique est une solution qui permet de ne rien changer d'autre : le modèle privilégié reste celui d'une propriété privée (1 adulte = 1 voiture), d'un usage individuel (1 voiture = 1 occupant), d'une vitesse de déplacement élevée (vitesse maximale > vitesse autorisée), d'un poids élevé (1 voiture = 100 vélos), d'un renouvellement fréquent (1 personne = N voitures).

La voiture électrique adresse de façon unidimensionnelle la question de l'empreinte carbone sans adresser les autres impacts environnementaux, comme l'exploitation des minerais nécessaires aux batteries, la pollution due à l'émission de particules par les pneus, l'infrastructure routière qu'elle rend nécessaire... La voiture électrique fait plusieurs paris, dont celui que la production de l'énergie stockée dans les batteries ne sera pas d'origine thermique (sinon le problème est déplacé, mais pas du tout résolu).

La voiture électrique permet enfin de prolonger les valeurs portée par l'objet technique voiture : puissance, modernité, automatisation ou encore réduction des limites spatiales et temporelles. Elle s'inscrit en cela dans le schéma dominant de nos sociétés contemporaines, que l'on peut résumer par le couplage de la croissance économique et du progrès technique.

### Propositions de la lowtechisation

Le scénario S1 de l'ADEME, "sobriété contrainte", s'articule autour de l'idée de restreindre nos développements technologiques, couplé à un scénario de décroissance d'un point de vue économique. L'enjeu d'une telle proposition et de faire vivre une alternative crédible en parallèle de la piste solutionniste. Si on statuait sur des investissements en recherche de même ordre entre les 4 scénarios alors le scénario S1 devrait bénéficier de quelque chose comme 10 milliards par an (l'investissement en recherche et développement en France est de 2.28% du PIB en 2020).

Penser ce scénario du point de vue de la conception des objets techniques implique une redirection de l'ingénierie (Monnin et al., 2021) c'est à dire une modification radicale des objectifs poursuivis par les industries afin de cesser de faire advenir des technologies qui contribuent au franchissement des limites planétaires et de réorienter leur activités vers la gestion des problèmes existants et la création de nouvelles technologies soutenables.

Nous définissons sur cette base la lowtechisation comme un processus de redirection de l'ingénierie afin de créer des outils plus soutenables, plus conviviaux et plus eudémonistes.

#### Imaginer un véhicule léger

En miroir de notre exemple précédent de la voiture électrique, on peut imaginer un objet basé sur des modes d'usage alternatifs. Un modèle de la propriété basé sur le partage, l'économie de fonctionnalité, voire la mise en commun (1 véhicule = N utilisateurs) ; un usage collectivisé (1 place = 1 occupant), une vitesse de déplacement et une autonomie adaptée aux agglomérations et trajets quotidiens en campagne (vitesse maximale = 50km/h, autonomie de 100km), un poids adapté en conséquence (1 kg objet = 1 kg humain), une réparabilité forte (durée de vie = durée de vie d'un humain). D'autres valeurs associées à l'objet peuvent alors être mises en avant : la maîtrise technique, la relocalisation, la simplicité, la durabilité.

Ce cahier des charges correspond à celui de l'Extrême Défi Mobilité proposé par l'ADEME. Il s'agit d'imaginer un véhicule 10 fois moins coûteux qu'une voiture, 10 fois plus léger, 10 fois plus durable, encore 10 fois moins consommateur, 10 fois plus simple à produire. Cela donnerait un véhicule de 150kg d'une durée de vie de 100 ans, avec 10kg de batterie pour 100km d'autonomie. Ce véhicule resterai aussi rapide qu'une voiture nous dit l'ADEME... Il s'agit pour résoudre cette étonnante équation, de considérer la vitesse généralisée au sens où Ivan Illich avait proposé d'intégrer au calcul de la vitesse le temps de travail nécessaire pour produire la voiture, les routes, payer le carburant, etc. (Héran, 2009). Ainsi une voiture moins coûteuse fait économiser du temps de travail et donc fait gagner en vitesse généralisée.

#### Faire des choix technologiques : il n'y a pas d'humain no-tech

La technique pose toujours problème, c'est par essence un pharmakon, un remède en même temps qu'un poison (au sens qu'à réactivé Bernard Stiegler depuis Platon). Mais on ne peut pas faire sans technique car la technique est anthropologiquement constitutive (Steiner, 2010). L'humanité advient en même temps qu'elle fait advenir la technique. En revanche, on peut faire des choix, la question de comment vivre avec quelles techniques, selon quel rapport de force et avec quel modèle de société peut-être posée. La recherche de l'ataraxie Épicurienne (état d'absence de trouble) est fondée sur une priorisation des besoins. L'approche proposée par Épicure en proposant de séparer les besoins naturels des non naturels pose d'emblée la question de la place de la technique. De la même façon, si la médiété chez Aristote n'est pas théorisée dans le domaine de la création technique, on pourra trouver que c'est un état des « œuvres bien réussies », telle qu'il est « impossible d'y rien retrancher ni d'y rien ajouter, voulant signifier par là que l'excès et le défaut détruisent la perfection ».

Depuis le XIXe siècle et le développement de l'industrialisation, les débats scientifiques et démocratiques sont peu présents quand aux choix techniques faits par la société. Ils s'expriment surtout par des rapports de force entre des bénéficiaires et des spoliés (dans le cas de Luddites en 1811-1812 qui s'opposent aux nouvelles machines à tisser qui les privent de leur métier) ou entre partisans du progrès technique et militants écologistes (extension du transport aérien au autoroutier, de l'agriculture intensive...)  (Éloge des mauvaises herbes, collectif, 2018). Dans le passé récent, à quelques occasions critiques, par exemple pour le nucléaire, des choix ont été fait de renoncer à des développements techniques par certains pays. On trouve d'autres exemples dans l'histoire de l'humanité, c'est la thèse défendue par David Graeber et David Wengrow (2021). Au Néolithique, les humains étaient en capacité de construire des modèles de société divers : cultivateur et/ou cueilleur, nomades et/ou sédentaires... C’est leur connaissance des alternatives, et non leur ignorance, qui conduisait ces peuples à se déterminer (également en fonction de leurs relations avec les sociétés voisines, que ce soit dans une logique de compétition ou de coopération). 

En conclusion, on fera l'hypothèse qu'il est possible et qu'il est nécessaire de faire des choix technologiques, et donc qu'il faut se doter de méthodes pour les accompagner.

#### Rediriger les processus d'innovation

La seconde hypothèse que nous faisons est qu'il est possible d'envisager l'innovation autrement que par le prisme du solutionnisme technologique, dans le cadre d'une redirection. Alexandre Monnin, Diego Landivar, et Emmanuel Bonnet (2021) emprunte ce concept à Tony Fry et l'oppose à la rupture d'une part et à la réforme d'autre part. La rupture fait l'hypothèse que l'on peut et doit faire complètement autrement, sans le capitalisme et les forces qu'il active actuellement. Cette option pose un problème de continuité, par exemple pour le démantèlement des artefacts du capitalisme (comme le nucléaire). La réforme fait l'hypothèse que le capitalisme est transformable : on garde les fondements du fonctionnement actuel que l'on améliore ou que l'on canalise. On trouve ainsi forgés les concepts tels que le développement durable ou le capitalisme vert, qui sont mobilisés aujourd'hui pour conserver la logique de croissance.

Le concept de redirection consiste à changer radicalement les objectifs que poursuit notre société, typiquement en organisant la fermeture d'infrastructures délétères tout en assumant la continuité avec le système actuel, en en acceptant l'héritage. L'idée d'héritage et fermeture (Monnin et al., 2021) s'envisage comme un projet pratique : se débarrasser de l'automobile individuelle, de la publicité ou de l'élevage intensif (pour ne prendre que trois exemples), suppose de gérer concrètement de l'existant : des bâtiments, des machines, des emplois, des formations associées en amont, des activités dépendantes en aval... Là où nos sociétés inventent et innovent pour dans une logique d'ouverture (pour alimenter la croissance) il s'agit d'innover et inventer pour fermer.

Il s'agit aussi, bien sûr de construire des alternatives en parallèle. C'est cette dimension que se propose d'adresse la lowtechisation. Fermer la voiture suppose d'inventer autres outils et modes de déplacement, qui ne soit pas un retour en arrière. Il n'y a pas de retour en arrière possible, on ne remonte pas dans le temps, mais il y a un futur qui ne poursuit pas nécessairement sa course en avant vers la croissance.

#### Hypothèse de redirection méthodologique

Une troisième hypothèse est qu'il est possible, pour innover dans une logique de lowtechisation, de rediriger des méthodes d'ingénierie existantes. L'intérêt principal de vouloir éviter la rupture est que la lowtechisation doit s'inscrire dans un contexte d'industrialisation : il est nécessaire de faire à suffisamment grande échelle pour gérer l'existant d'une part ; et les alternatives lowtechisées nécessitent des processus avancés, Philippe Bihouix (2014) nous rappelle ainsi qu'un objet d'apparence simple comme une chaîne de vélo ne relève pas de l'artisanat mais bien de l'industrie. L'enjeu est donc de faire advenir une industrie qui ne soit pas pilotée par le capitalisme contemporain tout en étant capable d'appuyer les projets de fermeture et l'émergence d'alternatives lowtechisées.

En pratique, notamment parce que nous avons appliqué la lowtechisation dans le contexte du numérique, nous proposons une méthode qui redirige les méthodes agiles via trois modifications majeures :

- une substitution des valeurs : on passe d'une logique coût/qualité/délais à soutenabilité/convivialité/eudémonisme ;
- une extension du domaine de l'ingénieur (leviers) : il ne se limite plus à répondre techniquement à un besoin qui pré-existerait, mais il assume son rôle de scénariste de modes de vie (imaginer) et son rôle politique (alerter) ;
- une attention portée à ces valeurs et leviers instrumentée au cœur de la démarche : celle-ci intègre une phase d'auto-évaluation réflexive a priori, lors de conception, avant que le produit n'existe, les ingénieurs passent leurs spécifications fonctionnelles au crible d'outils méthodologique qui permettent de poser et reposer les questions liées aux valeurs et leviers.

Nous ne détaillerons pas ces outils méthodologiques d'évaluation (que l'on peut retrouver sur le site lownum.fr), mais on pourra citer à titre d'illustration : les « 7 péchés du greenwashing » pour traquer les assertions écologiques non justifiées, l'outil « effet rebond » pour tenter d'anticiper les effets indirects, "l'empreinte fantôme" pour faire le lien entre des matières premières collectées, transformées et transportées d'un bout à l'autre du monde, ou encore le « diamant », en référence aux 5 facteurs d'effondrement de Jared Diamond.

#### Trois valeurs : soutenabilité, convivialité, eudémonisme

Nous proposons donc une autre approche méthodologique qui se fonde sur les valeurs de soutenabilité, convivialité et eudémonisme. Pour poser ces trois valeurs nous appuyons sur les travaux de Kate Raworth (2012), Philippe Bihouix (2014) et du Low-tech Lab (2023).

La théorie du Donut de Kate Raworth, économiste, propose d'adresser la notion d'espace juste et sûr pour les humains en insérant le développement économique entre une frontière environnementale (un plafond) et une frontière sociale (un plancher). Via ses 7 commandements des low-tech, Philippe Bihouix, insiste sur la remise en cause des besoins et la nécessité de durabilité, ce qui s'apparentent à la négociation des précédentes frontières. Puis, en tant qu'ingénieur, il ajoute une dimension technique, souhaitant des technologies que l'on peut connaître et maîtriser à travers leur conception, leur production et leur réparation. Le Low-tech Lab propose une définition qui reprend ces trois axes sociaux, environnementaux et technique : l'utilité, en lien avec la remise en cause des besoins, l'accessibilité, en lien avec l'appropriabilité de la technique par le plus grand nombre, et la durabilité, en lien avec la minoration de l'impact environnemental.

Schéma : https://framagit.org/stph.crzt/lownum/-/blob/main/schemas/lowtechnicisation-def-2-low-v3.svg

Légende : Les 3 valeurs de la lowtechisation

Ces trois termes que nous choisissons cherche à renvoyer à des processus, des méthodes, plutôt que des essences ou des états. Il ne s'agit pas de défendre une nature sacrée, un humanisme universel ou une technique autonome, de remplacer une mythologie par une autre, mais d'envisager l'articulation entre activités humaines, dispositifs techniques et ecosystèmes naturels afin de les penser ensemble et sous l'angle de valeurs alternatives à celle du capitalisme contemporain.

##### Soutenabilité

Côté environnemental, nous choisissons le terme de soutenabilité, plutôt que durabilité pour prendre nos distances avec le terme de développement durable, qui embarque les logiques de progrès et croissance. Les neufs limites planétaires (Rockström et al., 2009) eux-même repris par Kate Raworth forment une bonne définition de la notion de soutenabilité.

- Changement climatique
- Érosion de la biodiversité
- Perturbation des cycles de l'azote et du phosphore
- Modifications des usages des sols
- Pollution chimique
- Utilisation d’eau douce
- Diminution de la couche d’ozone
- Acidification des océans
- Aérosols atmosphériques

##### Convivialité

Côté technique, nous adoptons le terme de convivialité d'Ivan Illich (1973) : « L’outil est convivial dans la mesure où chacun peut l’utiliser, sans difficulté, aussi souvent ou aussi rarement qu’il le désire, à des fins qu’il détermine lui-même ». On s'inspirera également des 4 libertés du logiciel libre : utiliser sans restriction (exécuter dans le cadre d'un logiciel), étudier le fonctionnement (par exemple pour réparer), dupliquer et distribuer (par exemple pour faire bénéficier d'autres personnes de l'outil), modifier (pour améliorer, adapter...). Ces libertés sont aujourd'hui entravés par la loi (le droit d'auteur et les les brevets visent à interdire la plupart de ces opérations). Par exemple le brevetage des technologies du vivant permet de s'opposer à la réutilisation des semences produites (restriction d'usage) ainsi qu'aux échanges informels de semences entre producteurs (distribuer). Le droit d'auteur empêche par défaut la duplication d'un contenu culturel (dont les logiciels, articles scientifiques, plans...). 

L'outil convivial est utilisable par le plus grand nombre (en droit et en pratique), il ne crée pas de dépendance (il ajoute un sentier technique, sans entraver les autres chemins qui existaient ou existeront), il ne crée pas de hiérarchie entre les humains, il est partageable, facile à fabriquer (c'est à dire avec le « bon effet d'échelle » pour rependre la proposition n°5 de Philippe Bihouix), à modifier et à réparer.

##### Eudémonisme

Kate Raworth adosse les 12 besoins de bases de son plancher social aux objectifs du développement durable des Nations unies (objectifs du millénaire pour le développement formulés en 2000 et objectifs de développement durable, ODD, de 2015). Ces objectifs présentent l'inconvénient de mélanger des finalités et des moyens. Le développement est un moyen potentiel pour atteindre le bonheur, qui reste ici pensé comme une fin (on ne voit pas en quoi la consommation, même "responsable" est une fin en soi).

Philippe Bihouix et le Low-tech Lab s'adossent à la notion d'utilité et de besoins. Razmig Keucheyan (2019) rappelle que tous les besoins ne sont pas de même valeur et que certains besoins sont néfastes pour la personne (fumer) et/ou la société (s'armer). Par ailleurs les besoins ont tous une histoire, ils évoluent avec le temps. Xavier Guchet (2022) propose que les besoins sont à la fois naturels (ou biologiques, au sens de ressentis par le corps) et artificiels (ou construits, au sens de socio-techniques). La notion de besoin essentiel est donc vite inopérante dès lors que l'on sort de la stricte survie. 

Jeremy Bentham introduit le concept d'utilitarisme pour objectiver une recherche du bonheur ; ce que Stuart Mill va chercher à formaliser à la fois comme méthode (définir collectivement des finalités et chercher à les atteindre) et en déclinant des valeurs (la liberté des individus, l'égalité homme-femme, l'équité vis-à-vis des travailleurs, etc.).

Nous proposons dans cette lignée le terme d'eudémonisme, hérité de la pensée grecque que l'on peut traduire par recherche raisonnable du bonheur. Chez Aristote le bonheur est le bien suprême, le fondement des actions humaines, il s'ancre au niveau individuel et collectif. Aristote cite l'amitié, la justice, la vérité ou la sagesse comme vertu médianes dignes de conduire au bonheur. Pour Épicure le bonheur réside dans l'absence de trouble (l'ataraxie), et cet état se recherche en assouvissant ses besoins naturels et nécessaires (survivre) tout en étant frugal vis-à-vis de ses autres besoins (qui peuvent apporter du trouble, Épicure introduit un calcul des plaisirs).

