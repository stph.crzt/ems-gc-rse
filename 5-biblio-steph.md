## Bibliographie (Steph)

ADEME. « Extrême Défi ». Extrême Défi. Consulté le 1 novembre 2023. https://xd.ademe.fr/.
———. « Transition(s) 2050 : Choisir maintenant, agir pour le climat », 2022. https://librairie.ademe.fr/cadic/6531/transitions2050-rapport-compresse.pdf.
Ars Industrialis. « Pharmakon ». Consulté le 15 février 2016. http://arsindustrialis.org/pharmakon.
ADEME. « Extrême Défi ». Extrême Défi. Consulté le 1 novembre 2023. https://xd.ademe.fr/.
———. Les matériaux pour la transition énergétique, un sujet critique : Une analyse des principaux matériaux et métaux nécessaires au déploiement des EnR électriques et des véhicules. Prospective, Transitions 2050, 2022. https://librairie.ademe.fr/energies-renouvelables-reseaux-et-stockage/5351-prospective-transitions-2050-feuilleton-materiaux-de-la-transition-energetique.html.
———. « Transition(s) 2050 : Choisir maintenant, agir pour le climat », 2022. https://librairie.ademe.fr/cadic/6531/transitions2050-rapport-compresse.pdf.
Allard, Laurence, Alexandre Monnin, et Nicolas Nova. « Écologies du smartphone ». http://journals.openedition.org/lectures. Consulté le 12 septembre 2023. https://journals.openedition.org/lectures/54895.
Ars Industrialis. « Pharmakon ». Consulté le 15 février 2016. http://arsindustrialis.org/pharmakon.
Bihouix, Philippe. L’âge des low-tech : Vers une civilisation techniquement soutenable. Seuil., 2014.
Caillé, Alain. Critique de la raison utilitaire. La Découverte, 2003. https://doi.org/10.3917/dec.caill.2003.01.
Carnino, Guillaume. L’Invention de la science. Seuil., 2015.
Collectif. Éloge des mauvaises herbes : ce que nous devons à la ZAD. Édité par Jade Lindgaard, David Graeber, et Olivier Abel. Paris: Éditions Les Liens qui libèrent, 2018.
———. Second manifeste convivialiste : pour un monde post-néolibéral. Actes Sud. Questions de société, 2020.
Diamond, Jared. Effondrement. Folio, 2005.
Dyke, James, Robert Watson, et Wolfgang Knorr. « Climate Scientists: Concept of Net Zero Is a Dangerous Trap ». The Conversation, 22 avril 2021. https://theconversation.com/climate-scientists-concept-of-net-zero-is-a-dangerous-trap-157368.
Ellul, Jacques. « Réflexions sur l’ambivalence du progrès technique ». La Revue administrative 18, nᵒ 106 (1965): 380‑91.
Gautier, Vincent. « Un espace très... terre-à-terre ». Socialter, Le réveil des imaginaires, nᵒ Hors-série 8 (2020).
Goody, Jack. La raison graphique : la domestication de la pensée sauvage. Traduit par Jean Bazin et Alban Bensa. Éditions de Minuit, 1979.
Graeber, David, et David Wengrow. Au commencement était : Une nouvelle histoire de l’humanité. Les liens qui libèrent, 2021.
Guchet, Xavier. Du soin dans la technique : Question philosophique. ISTE Group, 2022.
Hersh, M. A. « Systemic Approaches and Technological Fixes ». IFAC-PapersOnLine, 16th IFAC Conference on Technology, Culture and International Stability TECIS 2015, 48, nᵒ 24 (1 janvier 2015): 71‑76. https://doi.org/10.1016/j.ifacol.2015.12.059.
Illich, Ivan. La convivialité. Seuil., 1973.
Keucheyan, Razmig. Les besoins artificiels : comment sortir du consumérisme. Éditions La Découverte. Zones, 2019.
Larousse, Pierre. « Progrès ». In Grand Dictionnaire universel du XIXe siècle, tome XIII:224, 1890.
Low-tech Lab. « Qu’est-ce que la low-tech ? », 2023. https://lowtechlab.org/fr/la-low-tech.
Masutti, Christophe. Affaires privées : Aux sources du capitalisme de surveillance. C&F Éditions., 2020.
Mill, John Stuart. Utilitarianism. Parker, Son&Bourn, West Strand., 1863.
Monnin, Alexandre, Emmanuel Bonnet, et Diego Landivar. Héritage et fermeture : une écologie du démantèlement. Éditions Divergences., 2021.
Monnin, Alexandre, Diego Landivar, et Emmanuel Bonnet. « Qu’est-ce que la redirection écologique ? » Horizons publics (blog), 2021. https://www.horizonspublics.fr/environnement/quest-ce-que-la-redirection-ecologique.
Parrique, Timothée. Ralentir ou périr, l’économie de la décroissance. Sueil., 2022.
Pitron, Guillaume. L’enfer numérique. Les Liens qui Libèrent., 2021.
Raworth, Kate. A safe and just space for humanity : can we live within the doughnut ? Oxfam Discussion Paper., 2012.
———. La théorie du donut : l’économie de demain en 7 principes. Plon., 2018.
Reporterre. « Jean Jouzel : Emmanuel Macron doit cesser de semer la confusion ». Reporterre, le média de l’écologie, 2023. https://reporterre.net/Jean-Jouzel-Emmanuel-Macron-doit-cesser-de-semer-la-confusion.
Rockström, Johan, Will Steffen, Kevin Noone, Åsa Persson, F. Stuart Chapin, Eric F. Lambin, Timothy M. Lenton, et al. « A Safe Operating Space for Humanity ». Nature 461, nᵒ 7263 (septembre 2009): 472‑75. https://doi.org/10.1038/461472a.
Roussilhe, Gauthier. « Les effets environnementaux indirects de la numérisation », 2022. https://gauthierroussilhe.com/articles/comprendre-et-estimer-les-effets-indirects-de-la-numerisation.
Steiner, Pierre. « Philosophie, technologie et cognition : état des lieux et perspectives ». Intellectica 53, nᵒ 54 (2010): 7‑40.
