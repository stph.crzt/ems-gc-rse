## Conclusion

Hypothèse. 
En questionnement ? le jour où ça casse le reste ne tient pas ?

exploitation de personnes : ultra-libéralisme, néo-colonialisme...
minerais en Afrique
assemblage de tel en Chine
travailleurs du clic (IA...)

travailleurs uberisés (au Nord)

Méthodes et exemples pour contrer ça


# TODO

#### Conclusion : évaluation réflexive, imagination et alerte

La lowtechisation est une méthode plutôt qu'une théorie, elle a un objectif pratique de redirection de la conception technique par les ingénieurs, au sens des acteurs humains qui construisent notre univers machinique. Cette méthode est notamment mobilisée à l'UTC dans un contexte pédagogique, en relation avec des acteurs socio-économiques qui peuvent ainsi en expérimenter les principes. Nous ne développerons pas cette méthode dans le cadre de cet article (on pourra la découvrir en détail via le site lownum.fr), même si nous l'illustrerons brièvement via la présentation de quelques exemples de projets menés avec les étudiants (en sous-partie 3).

Néanmoins, pour conclure sur la lowtechisation, revenons sur les 3 leviers sur lesquels elle se base ainsi que la démarche d'auto-évaluation réflexive.

L'ingénieur est définit, et dans une large mesure, se définit lui-même comme un acteur "purement" technique, au sens où il n'aurait pas la compétence, la légitimité, voire même le droit, de s'intéresser à ce qui sort du cadre purement technique. Il y a plusieurs hypothèses fautives dans cette posture, et sans les justifier ici, il est probable que ces erreurs participent des problèmes sociaux et environnementaux de notre monde contemporain. D'abord il n'y a pas de domaine technique pur, il n'y a que du technique en prise avec des humains, qui utilisent, qui construisent, qui extraient, et avec de la matière et des énergies, donc des questions environnementales. Même les choix vus par lui comme "purement techniques" (quelle matière, quelle énergie, quel assemblage, quelle taille, quel poids, quel résistance...) sont toujours porteurs de causes et conséquences socio-environnementales. Ne pas s'en préoccuper s'inscrit dans une logique cartésienne (à la fois au sens de la séparation des problèmes et de la distanciation à la nature), mais, si cette logique paraît peut-être efficace au moment de l'acte de conception, elle est aussi certainement aporétique en principe, et en tous cas en pratique si l'on prend une perspective spatiale et temporelle élargie (elle ne permet pas de comprendre ce qu'il se passe ailleurs et se qui se passera après).

Ensuite, on peut douter (soyons cartésiens à notre tour) du fait que l'ingénieur ne fait que répondre à un cahier des charges, à des besoins qui seraient formulés par d'autres. Le monde technique qu'il invente en le construisant rédige déjà les cahiers des charges de la machine suivante (le rôle joué par les géants du numérique dans la transformation de notre monde en est une illustration patente). Il est discutable que l'ingénieur n'ait que le rôle de remplir techniquement des fonctions formulées par d'autres. Pourquoi l'ingénieur ne serait-il pas en charge de discuter ces fonctions mêmes ? Il est méthodologiquement équipé pour cela et aussi légitime que le client ou le gestionnaire d'entreprise à le faire, qui n'ont pas plus accès à un besoin qui serait réel et tangible bien qu'encore abstrait. Et l'on notera que des domaines reconnus de l'ingénierie, comme l'analyse de la valeur, sont des démarches qui s'inscrivent au niveau fonctionnel).

Cette posture de "repli technique" conduit l'ingénieur à se concentrer sur la question de l'optimisation : si je dois produire un casque audio intra-auriculaire sans fil, alors mon travail est de trouver comment au mieux résoudre l'équation coût/délais/qualité dans ce contexte. Je ne suis pas là pour discuter les contraintes initiales (miniaturisation, sans fil) et encore moins la fonction "écouter un fichier audio en mobilité". Pourtant je serai en mesure d'expliquer que la miniaturisation excessive d'une liaison sans fil fragilise les composants et empêche leur recyclage ; ou encore que tel gain en terme de consommation d'énergie à l'usage est négligeable à côté de l'énergie consommée à la production ou au recyclage, et donc que c'est sur la durabilité des composants qu'il faudrait avant tout décider de travailler. Or s'aventurer sur ce terrain, c'est pour l'ingénieur accepter qu'il est un scénariste, un romancier, des modes de vie humains et également un acteur politique. 

Nous avons formalisé dans la méthode de lowtechisation les leviers d'imagination et d'alerte au même niveau que celui d'optimisation. Ainsi lors de la conception nous demandons à l'ingénieur de décrire des situations d'usage de ce qu'il envisage de construire via des histoires courtes (dans l'esprit de ce qui se pratique dans le cadre des méthodes agiles). Puis il formalise les fonctions que l'on "voit" en lisant cette histoire et pour chacune il explicite, dans une démarche prospective, en quoi elle relève d'une simple optimisation ou d'une reconfiguration des modes de vie (qui passe par l'imagination de nouveaux possibles accessibles individuellement, ou l'alerte de la communauté pour la prise de décision collectives).

« La redirection écologique prend acte du fait que l’ingénierie par optimisation ne permettra pas d’aligner nos organisations sur les limites planétaires. » nous disent Alexandre Monnin et ses collègues (2021), en ce sens il semble nécessaire que la conception technique intègre d'emblée les dimensions sociales et environnementales et que l'ingénieur refuse son confinement à la fable d'un "purement technique".