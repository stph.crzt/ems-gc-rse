## Fairphone

[stph : ça rejoint la partie que j'ai appelée "commerce équitable", n'est-ce pas ?]
Fairphone a demarré non pas comme un projet d'entreprise mais comme une campagne de sensibilisation menée par des activistes sur les minéraies dits  de conflit dans l'électronique grand public en 2010, notamment en République Démocratique du Congo (RDC). Après quelques années de militantisme, les personnes au centre de ce projet ont voulu depasser la prise de conscience et passer à l'etape superieure pour reellemnent créer une alternative. Elles fondent alors Fairphone, une entreprise sociale proposant des téléphones conçus d'une manière meilleure pour les gens qui les fabriquent et pour l'environnement, elles savaient que fabriquer un téléphone était le meilleur moyen de
comprendre les enjeux et influencer le changement de l'intérieur. Pour améliorer l'industrie électronique, il fallait devenir un une partie prenante de celle-ci.
Le Fairphone 1 a été lancé en décembre 2013, et le dernier, le Fairphone 5, en août 2023. Pas à pas, l'entreprise continue de travailler à sa mission de créer une électronique plus équitable, en développant de nouvelles relations
entre les personnes et leurs produits, et en montrant au reste de l’industrie ce qui est possible.

L’électronique et le numerique éthique nécessite une approche holistique vers la durabilité, le respect des personnes et
planète. Pour réaliser sa mission, Fairphone a une approche du changement en 3 étapes :-1:
1. Recherche et sensibilisation. En effectuant des recherches pour comprendre comment les chaînes d’approvisionnement fonctionnent et en quoi elles sont complexes, Fairphone communique de manière transparente sur ces decouvertes et informe le public sur problèmes auxquels est confrontée l’industrie.
2. Donner l’exemple. Étape par étape, Fairphone montre qu'il y a une nouvelle façon de fabriquer et d'utiliser les produits electroniques et numeriques. L'idee est de montrer qu'il est possible de rendre plus éthique ces produits, à savoir durable et respectant les gens qui les fabriquent, tout en etant commercialement rentable.
3. Motiver l'industrie à changer. Fairphone veut démontrer qu'un autre numerique est possible et motiver l’industrie électronique à remettre en question la mentalité de l'« utiliser-jeter » predominante dans l'indsutroie. FGairphine lie beaucoup de partenariats avec des acteurs diverdses de l'industrie electronique poiur les pousser à changer.

[stph : je mettrais ces 4 points en intro]
C’est donc la « théorie du changement » de Fairphone, qui est appliquée à quatre grands domaines d’impact clés :
• Longévité : créer des produits qui durent
• Circularité : reprise, réutilisation et recyclage
• Recherche de matériaux équitables
• Donner la priorité aux personnes impliquer dans le fabrication des produits electroniques en ameliorant leurs conditions de travail.



### Gouvernance (alternative au capitalisme)
The BCorps + entreprises de ESS (SCOP / Social Enterprises) qui ont comme objectifs pas uniquement la rentabiliuté financier mais egalememnt des indicateurs clés de performance (KPI) pour mesurer l'impact de nos produits.
Exemple de Fairphone -> 7 KPIs. 
KPI 1: FAIRPHONES SOLD
KPI 2: LONGEVITY SCORE
KPI 3: E-WASTE NEUTRALITY
KPI 4: FAIR MATERIALS
KPI 5: FAIR FACTORIES
KPI 6: INDUSTRY INFLUENCE SCORE
KPI 7: NET FINANCIAL RESULTS
De l'interieur, ce que ca veut dire de "suivre" ces KPIs
- rapport / actions
- temporalité (par mois,par annee, 3/4 ans)

### Réparabilité - Durabilité - Longevité

[rappeler que c'est la clé, l'impact n°1 c'est le matériel]
[lobbying Gafam "anti-durabilité"]

Numneriqye responsable = numerique durable
Indice de durabilité ou quand frilosité et ambitions s’affrontent...

Rêvons un peu : des produits électroniques open source, voire open hardware, ultra modulaires
composés de modules interopérables pour permettre la réutilisation de pièces détachées entre
différents modèles d’une même marque ou même entre différentes marques avec la standardisation
des composants et dont le support des processeurs serait garanti pendant 15 ans par les fabricants de
puces...
Même si nous sommes encore loin de ce rêve, petit à petit, la réparabilité fait son nid au cœur des
appareils électroniques : port USB C généralisé, première tentative pour rendre les batteries des
produits portables plus faciles à recycler et remplacer au niveau de l’UE... Un phénomène lié d’un côté
à l’évolution des mentalités des consommateurs qui face à l’urgence climatique commencent à se dire
que limiter le gaspillage en conservant et réparant ses appareils n’est pas si futile que cela, mais aussi
– et surtout ? – grâce à la législation qui exerce une pression de plus en plus forte sur les constructeurs
pour mettre fin à l’obsolescence programmée et aux bunkers impénétrables que sont devenus certains
appareils du quotidien. Malgré tout, le chemin reste semé d’embûches et la réticence qu’expriment
bon nombre de fabricants sur les questions de la réparabilité et de sa supposée antinomie avec
technologie de pointe et design léché nous font apparaître l’impérieuse nécessité pour les législateurs
de continuer à pousser des textes de lois contraignants. Et si l’on peut se féliciter que la France soit
pionnière sur la question depuis la mise en place en 2021 de l’indice de réparabilité et de la loi anti-
gaspillage pour une économie circulaire - qui prévoit à l’horizon 2024 qu’il devienne un indice de durabilité, notamment par l’ajout de nouveaux critères comme la robustesse ou la fiabilité des produits
- de nombreux aspects devraient être encore plus ambitieux.


Réparabilité → indépendance (possibilité de maîtriser) : convivialité
cf Eda : pub (obsolescence marketing) / panne / obsolescence programmé (technique)


### Ouverture (OS libre)

[un exemple pour lutter contre l'obsolescence, cf partie précédente]

#### La sobriété des OS
Très souvent les Systemes d'exploitaiton (Operating Systems en anglais = OS) libres sont très peu gourmands au niveau ressource. De fait, ils permettent de prolonger l’exploitation de terminaux qui autrement auraient été voués au recyclage. Linux est l’exemple le plus connu pour les ordinateurs, cet OS permet de redonner une nouvelle jeunesse à de nombreux appareils “en fin de vie” [1].
Ceci est aussi valable pour les smartphones, bien que moins connus. En effet, les mises à jour successives telles que conçues actuellement peuvent conduire au ralentissement des smartphones. Les OS libres constituent à ce moment-là un moyen de prolonger la durée de vie de l'appareil à condition que celui-ci soit compatible avec de tels OS. Les constructeurs de smartphones peuvent facilement permettre à la communauté du libre d’utiliser des OS libres en autorisant le déverrouillage du bootloader [2].

#### La liberté de reprendre la main sur le support
Aujourd’hui dans la grande majorité des cas, les constructeurs d’appareils ou de composants électroniques sont seuls décisionnaires de la fin de support des systèmes d’exploitation. Ceci progressivement limite les fonctionnalités des smartphones qui deviennent incompatibles avec les dernières versions des applications. De plus, ces appareils du jour au lendemain ne disposent plus de mise à jour de sécurité.
Par exemple, la fin du support d’Android 7 par Google condamne des millions de smartphones encore en utilisation sur le marché [3]. Aucune échappatoire si le smartphone ne dispose pas d’un bootloader déverrouillable et ne peut recevoir un OS alternatif.
La preuve par l’exemple de la plus-value des OS libres sur smartphone a été apportée par le Fairphone 2. Conçu en 2014 avec un processeur Qualcomm Snapdragon 801, suite à l'arrêt du
support par Qualcomm en 2015 sous android 6 le Fairphone 2 aurait été condamné s'il n’avait pas
été possible d’y installer un OS open source. En effet, avec l’aide de la communauté Lineage OS
(distribution open source basée sur Android et disponible pour un grand nombre d’appareils) les
équipes de Fairphone ont pu dans un premier temps déployer Android 7. Janvier 2020 cette fois-ci
c’est au tour de Google de stopper le support d’Android 7, condamnant les utilisateurs de Fairphone
2 à la vulnérabilité de leur appareil et à l’indisponibilité des versions récentes de leurs applications.
Dès février 2020 dans le cadre du service de notre coopérative Commown nous avons proposé une
mise à jour à nos clients sous Android 9 par l’intermédiaire de Lineage OS [4]. Si nous avons pu
réaliser cela et ainsi prolonger la durée de vie de nos appareils en exploitation c’est uniquement car
le Fairphone propose un bootloader déverrouillable. De plus, si l’utilisateur choisit de le déverrouiller
cela n'entraîne pas la fin de la garantie constructeur !
L’année dernière les équipes de Fairphone ont délivré une version d’Android 9 pour tous leurs
clients ayant un Fairphone 2, une nouvelle fois en collaboration avec la communauté de Lineage OS.
Et elles viennent de réitérer cette prouesse en annonçant la mise à jour du Fairphone 2 sous
Android 10 [5]. Cette longévité du support est un record sur le marché Android. Aucun autre
constructeur ayant des smartphones équipés d’un Qualcomm Snapdragon 801 ne s’est donné la
peine de maintenir à jour leurs appareils après la fin du support d’Android 6 [6].
Ainsi quand une entreprise décide unilatéralement de stopper le support de son appareil, ou si elle
fait faillite si ceux-ci sont “ouverts” la communauté peut prolonger la durée de vie des appareils.

#### Favorise le secteur du reconditionnement
La possibilité d’installer des OS libres, que ce soit sur smartphone ou PC, permet par ailleurs de
dynamiser le secteur du reconditionnement puisque un grand nombre de structures proposent
d'installer et de revendre des vieux terminaux remis à neufs sous un OS libre. Citons par exemple
AFB pour les PC ou encore la fondation /e/ pour les smartphones.

#### L’emprise de Google
Actuellement la double emprise de Google sur le marché des appareils Android paralyse la
concurrence. Google (comme son principal concurrent sur ce marché, Apple) a construit un
quasi-monopole de fait de la distribution des applications et des mises à jour des OS. Google assure
son emprise d’un côté sur les constructeurs de téléphone, de l’autre sur les éditeurs d’applications,
afin de maintenir ce monopole.
D’un côté, les constructeurs doivent obtenir l’accréditation de Google pour avoir le droit de
distribuer son OS avec les services permettant d’accéder à son magasin d’applications, le Play
Store. Sans ce sésame, le constructeur peut dire adieu au marché grand public, car les utilisateurs
n’ont alors plus accès aux applications pourtant écrites par des éditeurs qui ne sont pas liés à
Google (au sens capitalistique). Les constructeurs sont donc amenés à s’autocensurer et ne
peuvent pas pousser un OS alternatif libre (exemple : Fairphone n’a jamais proposé Fairphone Open
à la vente).
De l’autre côté, Google peut quasiment censurer des applications en les excluant de son magasin, le
Play Store [7]. Même sans pratiquer la censure, Google lie des services qu’il propose aux éditeurs
d’application (notifications, géolocalisation rapide, etc.) à leur distribution sur le Play Store. Grâce à
son monopole de fait sur les appareils Android (cf. son emprise sur les constructeurs), il n’existe pas
d’alternative pour ces services, car les fournisseurs n’auraient pas de marché, et ainsi les éditeurs
d’applications dont les utilisateurs ont besoin de tels services doivent rentrer dans le système

Google et s’astreindre aux conditions de leur présence dans le Play Store, et donc faire face à la
possibilité d’être censurés. Ceci conduit immanquablement à une nouvelle autocensure et un
blocage du marché par Google.
Cette emprise bloque l'émergence de solution alternative, puisque les constructeurs sont pieds et
poings liés à Google.



