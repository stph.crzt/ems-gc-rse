### Lowtechnisation et numérique en contexte pédagogique 

#### Introduction : le cours Lownum

Le cours "Lowtechnisation et numérique" a été ouvert à l'UTC en février 2023 pour les étudiantes et étudiants de première et seconde année après le bac (en début de cursus donc). Une autre version, en mode stage d'une semaine est proposée pour toucher d'autres étudiants depuis 2022. Avec Framasoft et Picasoft, deux associations d'éducation populaire au numérique, deux sessions ouvertes à distance et en ligne ont également été expérimentées en 2022 (mars-juin pour la première et octobre, décembre pour la seconde).

Le premier enjeu de ces formations est de travailler sur un projet en lien avec le numérique en mobilisant les concepts et méthodes de la lowtechisation. Il s'agit ici de projet de conception au sens de la défifition d'un produit nouveau, avant sa réalisation. Les projets n'intègrent pas de phase de réalisaton et se termine par la livraison d'une spécification fonctionnelle.

La réalisation n'est pas incluse, parce que :

- il est très difficile de travailler "pour de vrai" sur des objets techniques en situation pédagogique (par manque de temps, compétences, moyens...) ;
- la levée de la barrière technique, dans un premier temps de conception, permet de libérer son imagination.

Un second objectif est de tisser des liens avec des acteurs socio-économiques réels afin de diffuser les concepts d'une part et de donner vie à certaines de ces pistes de conception dans le monde industriel. Pour ce second objectif, les projets ont vocation à être menés en relations avec des acteurs tiers (éditeur logiciel, mairie, association...).?

Parmi les projets travaillés lors de ces occurrences : un navigateur Web éco-pédagogique, un ordinateur qui dure 50 ans, un système de comptabilité de l'empreinte environnementale des achats, ou une application de partage de véhicules sobres en campagne. On présentera ici un exemple pour illustrer la démarche. 

#### Lowtechisation du et par le numérique

Le lien entre lowtechisation et numérique est abordé selon deux points de vue dans le cadre de cette enseignement, qui peuvent être mobilisé conjointement, mais pas nécessairement.

- La low-technicisation *du* numérique cherche à lowtechiser les applications informatiques elles-mêmes. On citera par exemple le projet "Le Chaton Sportif" qui vise à produire un serveur d'applications informatiques libres grâce à l'énergie sportive de cyclistes.
- La low-technicisation *par le* numérique cherche à concevoir des applications informatiques en vue de lowtechiser la production d'objets tiers (numériques ou non). Le projet "8 Milliards d'internautes" imagine la mise en place d'un réseau contributif pour l'évaluation environnementale de sites web.

#### Exemple : le projet EcoCAD

EcoCAD est un logiciel de conception mécanique orienté low-tech, couplé avec un site web collaboratif destiné à construire et réparer chez soi. L'objectif est de proposer un logiciel adapté à l'utilisation par la plupart des citoyens, de partager les projets low-tech entre les utilisateurs pour les diffuser et les améliorer (sur le modèle du logiciel libre et de l'open hardware).

C'est donc d'abord un projet de lowtechisation par le numérique. Néanmoins c'est aussi un projet qui lotechise la CAO, en visant à la rendre accessible au plus grand nombre, mais également en intégrant d'emblée des contraintes comme : 

- la nécessité de fonctionner sur du matériel vétuste ou peu puissant, 
- dans de mauvaises conditions de fonctionement du réseau,
- dans le respect des libertés des utilisateurs (accès au logiciel, vie privée...), 
- l'indépendance vis-à-vis des multinationales du secteurs (GAFAM typiquement).

#### Exemples d'outils mobilisés au sein de la méthode 

Chaque projet est spécifié par la formalisation de 10 à 20 petites histoires qui décrivent une situation d'usage en une dizaine de lignes. Voic un extrait d'une de ces histoires pour le projet EcoCAD : 

« La coopérative Le vélo c'est rigolo fabrique des chaînes de vélo. Xavier, chargé de communication, pense que modéliser leur produit phare avec EcoCAD permettrait de lui donner plus de visibilité. Il demande à Sabrina, ingénieure, de modéliser et publier la pièce sur EcoCAD. Elle propose également un exemple de montage pour un blender à pédales. Cela fait gagner de la visibilité à l'entreprise, en plus de simplifier l'appropriation technique par les citoyens. »

Chaque histoire est associée à une ou plusieurs expressions fonctionnelles, elles-mêmes analysés selon un grille renvoyant aux valeurs et leviers de la lowtechisation.

« Le système permet aux entreprises de partager des modélisations 3D de leurs produits, ce qui leur donne de la visibilité et faciliter l'incorporation de leurs produits dans des projets low-tech.

- Valeur Soutenabilité : climat (transport)
- Valeur Convivialité : fabriquer, modifier, réparer, copier
- Valeur Eudémonisme : connaissance
- Levier Imagination: Le système permet la collaboration non marchande entre les individus et les entreprises.
- Levier Alerter: Le système montre la faisaibilité de l'appropriation citoyennne de la fabrication.
»

Le projet est également discuté via les outils d'auto-évaluation réflexive a priori, ici par exemple, un extrait de l'outil "Empreinte fantôme".

Schéma : empreinte-ecocad-1-zoom-netb.png

Légende : Extrait de l'outil empreinte fantôme appliqué au projet EcoCAD (contexte pédagogique)


#### Ouverture et diffusion

Les projets menés comportent une phase de publication qui demande aux participants de diffuser leur résultats sur un site web sous licence libre et de les présenter en direct sur la radio locale compiégnoise Graf'Hit. Ces deux exercices visent deux objectifs :

- donner un enjeu pour les projets, le contexte est pédagogique et le projet s'arrête avant la réalisation, mais pour autant ce n'est pas un pur exercice de style, on garde comme objectif d'agir sur le monde via l'information partagée ; 
- les projets peuvent se compléter de session en session pour prendre corps et in fine ouvrir sur des passages à la réalisation (le projet EcoCAD devraient ainsi se prolonger en laboratoire par un travail de doctorat).

Tous les travaux menés par les étudiants, ainsi que tous les supports pédagogiques sont accessibles via le site web lownum.fr.